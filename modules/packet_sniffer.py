
from scapy3k.all import sniff, wrpcap, rdpcap, TCP, hexdump

import sys
import re
import zlib

# regex to decode
# [{\+\s+|\+\w+|\d+|\W:+\\s+|\+\w+|\d+|\W}]

class PacketSniffer:

    def __init__ ( self, time_out = 60, filters = "tcp port 80", counts = 30 ):

        self.timeout = time_out
        self.filters = filters
        self.counts = counts
        self.packet = []

    def __str__ ( self ):

        if self.packet != []:

            try:

                for pkt in self.packet:

                    pkt.show ()
                    # re.findall( r'[{\+\s+|\+\w+|\d+|\W:+\\s+|\+\w+|\d+|\W}]', pkt.load )

            except:

                pass

            return ""

        return "Packet is empty, please start sniffing"

    def start ( self ):

        try:

            print ( "Sniffing..." )
            self.packet = sniff ( timeout = self.timeout, count = self.counts )
            print ( "Done!\n" )
            return

        except:

            print ( "Sniffing went wrong..." )
            sys.exit ( 0 )

    def __steal_email ( self, packet ):

        if packet [ TCP ].payload:

            mail_packet = str ( packet [ TCP ].payload )
            if 'user' in main_packet.lower () or 'pass' in mail_packet.lower ():
                print ( "[ * ] Server: %s" % packet [ IP ].dst )
                print ( "[ * ] %s" % packet [ TCP ].payload )

        return

    def steal_email ( self ):

        self.packet = sniff ( filter="tcp port 110 or tcp port 25 or tcp port 143", prn=self.__steal_email, store=0 )

        return

    def dump_tpc_packet ( self, path = "./dumps/dump.pcap" ):

        wrpcap ( path, self.packet )
        print ( "You can find it at: %s" %path )

        return

    def parse_packet_payload ( self ):

        sessions = self.packet.sessions ()

        for session in sessions:

            http_payload = ""

            for packet in sessions [ session ]:

                print ( packet [ TCP ].payload )

                # print ( packet [ TCP ].payload )
                # bytes ( packet [ TCP ].payload )

                # self.__striptxt_packet ( packet )

        return

    def __parse_http_headers ( http_payload ):

        try:

            headers_raw = http_payload[ :http_payload.index( "\r\n\r\n" ) + 2 ]
            regex = u"(?:[\r\n]{0,1})(\w+\-\w+|\w+)(?:\ *:\ *)([^\r\n]*)(?:[\r\n]{0,1})"
            headers = dict ( re.findall ( regex, headers_raw, re.UNICODE ) ) 

            print ( headers )
            return headers

        except:

            return None

        if 'Content-Type' not in headers:

            return None

        return headers

    def __extract_text ( headers, http_payload ):

        text = None
        
        try:

            if 'text/plain' in headers [ 'Content-Type' ]:

                text = http_payload[ http_payload.index("\r\n\r\n")+4: ]

                try:

                    if 'Accept-Encoding' in headers.key ():

                        if headers [ 'Accept-Encoding' ] == 'gzip':

                            text = zlib.decompress ( text, 16 + zlib.MAX_WBITS )
                        
                        elif headers [ 'Content-Encoding' ] == 'deflat':

                            text = zlib.decompress ( text )

                except:

                    pass

        except:

            return None

        return text

    def __extract_payload ( self, http_headers, payload ):

        text = None
        payload_type = http_headers [ "Content-Type" ].split ( "/" ) [ 1 ].split ( ";" ) [ 0 ]

        try:

            if "Content-Encoding" in http_headers.keys ():

                if http_headers [ "Content-Encoding" ] == "gzip":

                    text = zlib.decompress ( payload, 16 + zlib.MAX_WBITS )

                elif http_headers [ "Content-Encoding" ] == "deflate":

                    text = zlib.decompress ( payload )

                else:

                    text = payload

            else:

                text = payload

        except:

            pass

        return text

    def __striptxt_packet ( self, packet ):

        if packet [ TCP ].dport == 80:

            payload = bytes ( packet [ TCP ].payload )

            http_header_exists = False

            try:

                http_header = payload [ payload.index ( b"HTTP/1.1" ) : payload.index ( b"\r\n\r\n" ) + 2 ]
                
                if http_header:

                    http_header_exists = True

            except:

                pass

            if http_header_exists:

                http_header_raw = payload [ :payload.index ( b"\r\n\r\n" ) + 2 ]

                http_header_parsed = dict ( re.findall ( r"(?P<name>.*?): (?P<value>.*?)\r\n" ), http_header_raw.decode ( "utf8" ) )

                if "Content-Type" in http_header_parsed.keys ():

                    if "text" in http_header_parsed [ "Content-Type" ]:

                        text_payload = payload [ payload.index ( b"\r\n\r\n" ) + 4 : ]

                        if text_payload:

                            print ( self.__extract_payload ( http_header_parsed, text_payload ) )


# fake data for sake of demo when regular expression goes out of way to decode payload

fake_payload_decoded = [{

    "id": "1",
    "username": "johny"

}]