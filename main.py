
from modules.packet_sniffer import PacketSniffer
import sys

def main ():

    try:

        packetSniffer = PacketSniffer ()

        print ( "Packet Sniffer Script Successfully Started" )
        print ( "------------------------------------------" )

        while ( True ):

            user_input = input ( "Sniffing ? Y/N: " ).upper ()

            if user_input == "N":

                break;

            user_input = input ( "Change count ( %s ) ? Y/N: " %packetSniffer.counts ).upper ()

            if user_input == "Y":

                packetSniffer.counts = int ( input ( "Enter integer number of counts: " ) )

            packetSniffer.start ()

            print ( packetSniffer )

            # sessions = packetSniffer.packet.sessions ()

            user_input = input ( "Check the payload ? Y/N: " ).upper ()

            # if user_input == "Y":

                # packetSniffer.parse_packet_payload ()

            user_input = input ( "Dump the latest captured packet ? Y/N: " ).upper ()

            if user_input == "Y":

                packetSniffer.dump_tpc_packet ();

    except:

        print ( "Something went wrong..." )

    print ( "------------------------------------------" )
    print ( "Script Exited 0" )
    sys.exit ( 0 )

if __name__ == "__main__":

    main ()